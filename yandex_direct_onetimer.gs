// table headers, adjust for your needs
var parameters = ['Date', 'CampaignType', 'CampaignId', 'CampaignName', 'Impressions', 'Clicks', 'Cost', 'Ctr', 'AvgImpressionPosition']

/**
 * Create a menu
 */
function onOpen() {
    var ui = SpreadsheetApp.getUi();
    ui.createMenu('Get Statistics')
        .addItem('Get Clients', 'client_runner')
        .addItem('Get Stats', 'report_runner')
        .addToUi();
}

/**
 * Get Yandex Direct Statistics
 * From - first date in range
 * DateTo - last date in range
 */
function getYdStats(client, start_date, end_date, parameters) {
    var base_url = PropertiesService.getScriptProperties().getProperty('yd_base_url');
    var yd_token = PropertiesService.getScriptProperties().getProperty('yd_token');
    //var client_name = PropertiesService.getScriptProperties().getProperty('yd_client_login');  
    //var end_date = new Date()
    //var start_date = new Date(end_date.getTime() - 1000 * 60 * 60 * 24 * 7) // 7 days ago
    var report_url = base_url + 'reports'
    if (typeof start_date.getMonth != 'function') {
      var dates = parse_dates(start_date)
      var start_date = dates['start_date']
      var end_date = dates['end_date']
    }
    Logger.log(start_date)
    Logger.log(end_date)
    Logger.log(parameters)
    var payload = {
        'params': {
            'SelectionCriteria': {
                'DateFrom': Utilities.formatDate(start_date, "GMT+3", "yyyy-MM-dd"),
                'DateTo': Utilities.formatDate(end_date, "GMT+3", "yyyy-MM-dd")
            },
            'FieldNames': parameters,
            'ReportName': client + 'report_final_tests2' + Utilities.formatDate(new Date(), "GMT+3", "dd-MM-yyyy"),
            'ReportType': 'CAMPAIGN_PERFORMANCE_REPORT',
            'DateRangeType': 'CUSTOM_DATE',
            'Format': 'TSV',
            'IncludeVAT': 'YES',
            'IncludeDiscount': 'NO'
        }
    }
    var options = {
        'headers': {
            'Authorization': 'Bearer ' + yd_token,
            'Accept-Language': 'ru',
            'Client-Login': client,
            'skipReportHeader': 'true',
            'skipColumnHeader': 'true',
            'skipReportSummary': 'true',
            'processingMode': 'offline'
        },
        'muteHttpExceptions': true,
        'payload': JSON.stringify(payload)
    }
    var yd_data = [];
    while (true) {
        var response = UrlFetchApp.fetch(report_url, options);
        if (response.getResponseCode() == 200) {
            //Logger.log(response.getContentText());
            for (i = 0; i < parseInt(response.getContentText().split('\n').length - 1); i++) {
                var tmp = response.getContentText().split('\n')[i].split('\t')
                var tmp_arr = []
                for (k=0; k<tmp.length; k++) {
                  tmp_arr.push(convert(tmp[k], k)) // Cleaning metrics and forming an array (basically row)
                }
                yd_data.push(tmp_arr);
            }
          return {'start_date': start_date, 'data': yd_data};
        } else if (response.getResponseCode() == 201) {
            Logger.log('yandex report queued');
            Logger.log(response.getAllHeaders());
            Utilities.sleep(parseInt(response.getAllHeaders()['retryin']) * 1000)
        } else if (response.getResponseCode() == 202) {
            Logger.log('yandex report is being processed');
            Logger.log(response.getAllHeaders());
            Utilities.sleep(parseInt(response.getAllHeaders()['retryin']) * 1000)
        } else {
            Logger.log('failed ' + response);
            break
        }
    }
}

/**
 * WIP: Get Yandex Direct Clients list
 */
function getYdClients() {
    var base_url = PropertiesService.getScriptProperties().getProperty('yd_base_url');
    var yd_token = PropertiesService.getScriptProperties().getProperty('yd_token');
    var report_url = base_url + 'agencyclients'
    var payload = {
        'method': 'get',
        'params': {
          'FieldNames': ['Login', 'ClientInfo'],
          'SelectionCriteria': {},
        }
    }
    var options = {
        'headers': {
            'Authorization': 'Bearer ' + yd_token,
            'Accept-Language': 'ru'
        },
        'muteHttpExceptions': true,
        'payload': JSON.stringify(payload)
    }
    var yd_data = [];
    while (true) {
        var response = UrlFetchApp.fetch(report_url, options);
        if (response.getResponseCode() == 200) {
            var clients = JSON.parse(response.getContentText())['result']['Clients'];
            for (i=0; i<clients.length; i++) {
              yd_data.push([clients[i]['Login'], clients[i]['ClientInfo']])
            }
            return yd_data        
        } else {
            Logger.log('failed ' + response);
            break
        }
    }
}
        
/**
 * Writer
 * sheet_name - sheet name to put the data on
 * param data - actual data to be written
 * param headers - table headers for the data
 * param first_cell - where to put the table
 */
function writer(sheet_name, data, headers, first_cell) {
    Logger.log('first_cell: ' + first_cell)
    if (first_cell == '1,1') {
      Logger.log('got [1, 1] so add headers and write to sheet')
      SpreadsheetApp.getActiveSpreadsheet().getSheetByName(sheet_name).clearContents()
      data.unshift(headers)
    }
    var rng = SpreadsheetApp.getActiveSpreadsheet().getSheetByName(sheet_name).getRange(first_cell[0], first_cell[1], data.length, headers.length);
    rng.setValues(data);
}

/**
 * Main
 */
function client_runner() {
    var yd_clients = getYdClients()
    var parameters = ['client_login', 'client_info']
    writer('Dictionaries', yd_clients, parameters, [1,1])
}

/**
 * Turn TSV data to dict and clean it
 * unused - delete later
 */
function data_dict(data, parameters) {
    var data_dict_list = []
    for (i = 0; i < data.length - 1; i++) {
        var tmp = data[i].split('\t')
        var data_dict = {}
        for (k = 0; k < parameters.length - 1; k++) {
            data_dict[parameters[k]] = tmp[k]
        }
        data_dict_list.push(data_dict)
    }
    Logger.log(data_dict_list)
}

/**
 * Converter, add rules for other metrics freely
 */
function convert(value, index) {
  var type = parameters[index]
  switch (type) {
    case 'Cost':
      var res = parseInt(value) / 1000000 / 1.20
      break;
    case 'AvgImpressionPosition':
      var res = value.replace('.', ',').replace('--', 0)
      break;
    case 'Ctr':
      var res = value.replace('.', ',').replace('--', 0)
      break;
    default:
      var res = value
      break;
  }
  return res
}

/*
 * Report runner
 */
function report_runner() {
    var ss = SpreadsheetApp.getActiveSpreadsheet();
    var work_sheet = ss.getSheetByName('Settings')
    var last_row = work_sheet.getLastRow()
    var all_data = work_sheet.getDataRange().getValues();
    var headerRow = all_data[0];
    var report_data = [];
    for (var row = 1; row < all_data.length; row++) {      
        var obj   = {};
        for(var rowColumn=0;rowColumn<headerRow.length;rowColumn++){
            obj[headerRow[rowColumn]]=all_data[row][rowColumn];
        }    
        report_data.push(obj);
    }
    for (n=0; n<report_data.length; n++) {        
        var sheet_name = report_data[n]['sheet_name']
        if (!ss.getSheetByName(sheet_name)) {
            ss.insertSheet(sheet_name)
        }
        var yd_stats = getYdStats(report_data[n]['client'], report_data[n]['start_date'], report_data[n]['end_date'], report_data[n]['metrics'].split(','))
        var f_cell = get_first_cell(sheet_name, yd_stats['start_date'], report_data[n]['recurring'])
        Logger.log('recurring no: ' + report_data[n]['recurring'])
        Logger.log('f_cell: ' + f_cell)
        Logger.log('yd_stats["start_date"]: ' + yd_stats['start_date'])
        writer(sheet_name, yd_stats['data'], report_data[n]['metrics'].split(','), f_cell)
    }
}

/**
 * Getting start and end dates from last_week, last_N_days, last_month, this week
 */
function parse_dates(period) {
  //period = 'this_week'
  var today = new Date()
  if (period == 'last_week') {        
    var day = today.getDay()
    if(today.getDay() == 0){
        var start_date = new Date(today.getTime() - 1000 * 60 * 60 * 24 * 13)
        var end_date = new Date(today.getTime() - 1000 * 60 * 60 * 24 * 7)
    } else {
        var start_date = new Date(today.getTime() - 1000 * 60 * 60 * 24 * (day + 6))
        var end_date = new Date(today.getTime() - 1000 * 60 * 60 * 24 * day)
    }
    return {'start_date': start_date, 'end_date': end_date}
  } else if (/^last_\d{1,2}_days$/.test(period)) {
    var n = parseInt(/^last_(\d{1,2})_days$/.exec(period)[1])
    var start_date = new Date(today.getTime() - 1000 * 60 * 60 * 24 * n)
    var end_date = new Date(today.getTime() - 1000 * 60 * 60 * 24 * 1)
    return {'start_date': start_date, 'end_date': end_date}
  } else if (period == 'last_month') {
    var start_date = new Date(new Date(today.setDate(0)).setDate(1))
    var end_date = new Date(new Date().setDate(0))
    return {'start_date': start_date, 'end_date': end_date}
  } else if (period == 'this_week') {
    var day = today.getDay()    
    if(today.getDay() == 0) {
        var start_date = new Date(today.getTime() - 1000 * 60 * 60 * 24 * 6)
        var end_date = new Date(today.getTime())
    } else {
        var start_date = new Date(today.getTime() - 1000 * 60 * 60 * 24 * (day-1))
        var end_date = new Date(today.getTime())
    }
    return {'start_date': start_date, 'end_date': end_date}
  } 
}

/**
 * Getting first cell on according sheet - used to correctly "update" the data or append
 */
function get_first_cell(sheet_name, start_date, recurring) {
  var ss = SpreadsheetApp.getActiveSpreadsheet().getSheetByName(sheet_name)
  var data_range = ss.getDataRange();
  var last_row = data_range.getLastRow();
  Logger.log('start date: ' + start_date)
  Logger.log('data range last row: ' + last_row)
  if (recurring == 'yes' && last_row == 1) {
    return [1, 1]
  } else if (recurring == 'yes' && last_row > 1){
    for (i=2; i<last_row; i++) {
      if (Utilities.formatDate(ss.getRange(i, 1).getValue(), "GMT+3", "yyyy-MM-dd") == Utilities.formatDate(start_date, "GMT+3", "yyyy-MM-dd")) {
        return [i, 1]
      }      
    }
    return [last_row+1, 1]
  } else {
    return [1, 1]
  }
}



// TODO
// Oauth2
// Function to get Clients list - done
// Dictionaries: Metrics list for CAMPAIGN_PERFORMANCE_REPORT - WIP
// Make report working with report configs from the 'Settings' list - done
// Make it work with multiple report - done
// Make it work with start and end date and with last_N_days - done
// One-time and recurring options - done
// Doc - how to create a trigger -WIP